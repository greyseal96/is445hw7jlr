﻿using IS445HW7JLR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IS445HW7JLR.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            ViewBag.Title = "Home";
            return View();
        }

        //
        // GET: /Home/WebFormDemo

        public ActionResult WebFormDemo()
        {
            ViewBag.Title = "Web Form Demo";

            Reservation rsrv = new Reservation();
            rsrv.ReservationArrivalDT = DateTime.Now;
            rsrv.ReservationGuest = new Guest();
            rsrv.ReservationRoom = new Room();
            
            return View(rsrv);
        }

        //
        // POST: /Home/WebFormDemo

        [HttpPost]
        public ActionResult WebFormDemo(Reservation rsrv)
        {
            return View(rsrv);
        }

        //
        // GET: /Home/AboutMe

        public ActionResult AboutMe()
        {
            ViewBag.Title = "About Me";
            return View();
        }
    }
}
