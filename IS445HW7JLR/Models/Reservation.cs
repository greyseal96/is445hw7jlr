﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IS445HW7JLR.Models
{
    public class Reservation
    {
        public Guest ReservationGuest { get; set; }
        public Room ReservationRoom { get; set; }

        [Required(ErrorMessage = "Please enter a date and time for arrival")]
        public DateTime ReservationArrivalDT { get; set; }

        [Range(1,30)]
        [Required(ErrorMessage = "Please enter the number of nights for the reservation")]
        public int ReservationStayNights { get; set; }

        [Range(0,30)]
        [Required(ErrorMessage = "Please enter the number of guests for the reservation")]
        public int ReservationNumGuests { get; set; }
        public string ReservationPromoCode { get; set; }

        [DataType(DataType.MultilineText)]
        [AdditionalMetadata("rows", 5)]
        [AdditionalMetadata("cols", 40)]
        [AdditionalMetadata("placeholder", "Enter your special notes here")]
        public string ReservationNotes { get; set; }
    }
}