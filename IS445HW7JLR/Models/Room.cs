﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IS445HW7JLR.Models
{
    public enum RoomType
    {
        Luxury,
        Standard,
        Economic
    }

    public class Room
    {
        [UIHint("Enum")]
        public RoomType RmType { get; set; }
        public int MaxOccupancy { get; set; }
        public double BasePrice { get; set; }
    }
}