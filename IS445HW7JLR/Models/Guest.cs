﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IS445HW7JLR.Models
{
    public class Guest
    {
        [UIHint("EditorForWithPlaceholder")]
        [Required(ErrorMessage="Please enter your name")]
        [StringLength(50, ErrorMessage="The name can only be 50 characters")]
        [Display(Prompt="Firstname Lastname")]
        public string FullName { get; set; }

        [Required(ErrorMessage="Please enter an email address")]
        [StringLength(100, ErrorMessage="The email address can only be 100 characters")]
        [RegularExpression(@"^[A-Za-z0-9._%+-]+@(?:[A-Za-z0-9-]+\.)+[A-Za-z]{2,6}$", ErrorMessage = "Please enter a valid email address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage="Please enter a phone number")]
        [StringLength(100, ErrorMessage="The phone number can only be 100 characters")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}